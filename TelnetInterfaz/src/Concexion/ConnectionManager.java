
package Concexion;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectionManager {
    
    private static final String HOST  = "10.16.10.254";
    //private static final String HOST  = "192.168.50.245";
    private static final String DATABASE = "NewsoftvWeb";
    private static final String USER = "sa";
    private static final String PASS = "0601x-2L";

    private static Connection con;

    //Método para obtener una conexion a la BD
    public static Connection getConnection(){
        try{
             Class.forName("net.sourceforge.jtds.jdbc.Driver");
            String url = "jdbc:jtds:sqlserver://" + HOST + "/"+ DATABASE;
            //con = DriverManager.getConnection(url, USER, CipherUtils.decrypt(PASS));
            con = DriverManager.getConnection(url, USER, PASS);
        }catch(SQLException sqle){
            System.out.println("Excepcion al tratar de establecer la conexion" +
                    " a la base de datos");
            sqle.printStackTrace();
        }catch(ClassNotFoundException cnfe){
            System.out.println("Excepcion al tratar de cargar el driver de " +
                    "conexion");
            cnfe.printStackTrace();
        }
        
        return con;
        
    }
    
    
    //Método para cerrar la conexion a la BD
    public static void closeConnection(Connection c, ResultSet rs, Statement st){
        if(c != null){
            try{
                c.close();
            }catch(SQLException sqle){
                System.out.println("Excepcion al tratar de cerrar la conexion: " +
                        "objeto Connection no se pudo cerrar");
                sqle.printStackTrace();
            }
        }
        
        if(rs != null){
            try{
                rs.close();
            }catch(SQLException sqle){
                System.out.println("Excepcion al tratar de cerrar la conexion: " +
                        "objeto ResultSet no se pudo cerrar");
                sqle.printStackTrace();
            }
        }
        
        if(st != null){
            try{
                st.close();
            }catch(SQLException sqle){
                System.out.println("Excepcion al tratar de cerrar la conexion: " +
                        "objeto Statement no se pudo cerrar");
                sqle.printStackTrace();
            }
        }
    }
}
