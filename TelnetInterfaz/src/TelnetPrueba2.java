
import java.io.IOException;
import org.apache.commons.net.telnet.TelnetClient;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.SocketException;

public class TelnetPrueba2 {

    //Clase de telnet para java
    private TelnetClient telnet = new TelnetClient();
    private InputStream in;
    private PrintStream out;
    // Comprobamos el simbolo del prompt para asegurarnos logueo, “$” , “#” o “>”, cada servidor nos pondra el que el desee
    private String prompt = "#";

    public TelnetPrueba2(String server, String user, String password, String popmptconexion) throws IOException {
        try {
            // Connect to the specified server
            //server = ip, nombre del servidor telnet
            Main2.jTextArea_conexion.setText("");
            telnet.connect(server, 23);

            // Get input and output stream references
            in = telnet.getInputStream();
            out = new PrintStream(telnet.getOutputStream());
            
            // Log the user on
            //write("");
            //readUntil("login: ");
            write(user);
            readUntil("Password: ");
            write(password);

            // Advance to a prompt
            //readUntil(popmptconexion + " ");
            readUntil(popmptconexion);

        } catch (SocketException e) {
            Main2.jTextArea_comandos.append(System.getProperty("line.separator"));
            Main2.jTextArea_comandos.append("TIME OUT ");
        }
           
    }

    public TelnetPrueba2(String server, String user, String password, String popmptconexion, String tipoonu) throws IOException {
        try {
            // Connect to the specified server
            //server = ip, nombre del servidor telnet
            Main2.jTextArea_conexion.setText("");
            telnet.connect(server, 23);

            // Get input and output stream references
            in = telnet.getInputStream();
            out = new PrintStream(telnet.getOutputStream());
            // Log the user on
            //write("");
            //readUntil("login: ");
            //Main2.jTextArea_conexion.append("onu tipo 3");
            if (tipoonu.equals("3")) {
                write(user);
                readUntil("Password: ");
                write(password);
                // Advance to a prompt
                //readUntil(popmptconexion + " ");
                readUntil(popmptconexion);
            }

        } catch (SocketException e) {
            Main2.jTextArea_comandos.append(System.getProperty("line.separator"));
            Main2.jTextArea_comandos.append("TIME OUT ");
            e.printStackTrace();
        }
    }

    public void setPrompt(String prompt) {
        this.prompt = prompt;
    }

    public void su(String password) {
        try {
            write("su");
            readUntil("Password: ");
            write(password);
            prompt = ">";
            readUntil(prompt + " ");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String readUntil(String pattern) {
        try {
            char lastChar = pattern.charAt(pattern.length() - 1);
            StringBuffer sb = new StringBuffer();
            boolean found = false;
            char ch = (char) in.read();
            while (true) {
                System.out.print(ch);
                Main2.jTextArea_conexion.append(ch + "");
                sb.append(ch);
                if (ch == lastChar) {
                    if (sb.toString().endsWith(pattern)) {
                        return sb.toString();
                    }
                }
                ch = (char) in.read();
            }
        } catch (Exception e) {
            out.close();
            Main2.jTextArea_comandos.append(System.getProperty("line.separator"));
            Main2.jTextArea_comandos.append("Time out ");
            try {
                    in.close();
                } catch (IOException y) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return null;
    }

    public String readUntilReboot(String pattern) {
        try {
            char lastChar = pattern.charAt(pattern.length() - 1);
            StringBuffer sb = new StringBuffer();
            boolean found = false;
            char ch = (char) in.read();
            while (true) {
                System.out.print(ch);
                Main2.jTextArea_conexion.append(ch + "");
                sb.append(ch);
                return sb.toString();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void write(String value) {
        try {
            out.println(value);
            out.flush();
            System.out.println(value);
            Main2.jTextArea_comandos.append(System.getProperty("line.separator"));
            Main2.jTextArea_conexion.append(value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String sendCommand(String command) {
        try {
            write(command);
            return readUntil(prompt + " ");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String reincio(String command) {//funcion que recibe el rebot para el reinicio de los equipos
        try {
            write(command);
            return readUntilReboot(prompt + " ");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void disconnect() {
        try {
            telnet.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
